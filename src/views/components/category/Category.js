import React, { useState, useEffect } from "react";
import ScrollMenu from "react-horizontal-scrolling-menu";

import "./Category.css";

const Category = (props) => {

  const Arrow = ({ text, className }) => {
    return <div className={className}>{text}</div>;
  };

  const onSelect = key => {
    props.fetchProduct(key)
  };

  const [menuItemData, setMenuItem] = useState([])

  useEffect(() => {
    const tempMenuHtml = Menu()
    setMenuItem(tempMenuHtml)
  }, [props.categoryList])

  const Menu = () => {
    let temp = [...props.categoryList]
    temp = temp.map(el => {
      const { category_name, category_id, category_image } = el;
      
      return <MenuItem text={category_name} categoryId={category_id} img={category_image} key={category_id} />;
    });
    return temp
  }

  return (
    <div>
      {
        (props.categoryList && (props.categoryList.length > 0) &&
        <ScrollMenu
          alignCenter={false}
          // arrowLeft={Arrow({ text: "<", className: "arrow-prev" })}
          // arrowRight={Arrow({ text: ">", className: "arrow-next" })}
          clickWhenDrag={false}
          data={menuItemData}
          dragging={true}
          hideArrows={true}
          hideSingleArrow={true}
          onSelect={onSelect}
          scrollToSelected={false}
          transition={1}
          translate={0}
          wheel={true}
        />
        )
      }
    </div>
  );
};

export default Category;

const MenuItem = ({text, categoryId, img}) => {
  return <div
    className={`menu-item`}
    >
      <div className="image">
        <img className="imgCss" src={img} alt="img" />      
        <h2><span>{text}</span></h2></div><br/>
    </div>
};
