import React, { useState, useEffect } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import ScrollMenu from "react-horizontal-scrolling-menu";
import { Row, Col, Container } from "react-bootstrap";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import InputBase from "@material-ui/core/InputBase";
import FormControl from "@material-ui/core/FormControl";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

import "./Product.css";

const BootstrapInput = withStyles((theme) => ({
  root: {
    "label + &": {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #ced4da",
    fontSize: 16,
    padding: "10px 26px 10px 12px",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
    "&:focus": {
      borderRadius: 4,
      borderColor: "#80bdff",
      boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)",
    },
  },
}))(InputBase);

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
  },
}));

const Product = (props) => {
  const defaultProductLimit = 3

  const classes = useStyles();

  const [limit, setLimit] = useState(defaultProductLimit);

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const toggleLimit = () => {
    if (!limit) {
      setLimit(defaultProductLimit);
    } else {
      setLimit(false);
    }
  };

  const toggleCategory = (id) => {
    props.fetchProduct(id);
    handleClose()
  };

  const filterProduct = props.productList.filter((item, ref) => {
    return !limit || ref < limit;
  });
  return (
    <div ref={React.createRef()}>
      {filterProduct && (filterProduct.length > 0) && (
        <Grid container spacing={2}>
          {filterProduct.map((product) => (
            <Grid key={product.id} item md={4} sm={12}>
              <Grid container spacing={1}>
                <Grid className="align-center" item md={4}>
                  <img src={product.image_urls.x120} alt="img" />
                </Grid>
                <Grid item md={7}>
                  <div className="itemDiv">
                    <h4 className="customHead">{product.name}</h4>
                  </div>
                  <div className="itemDiv">
                    <span className="mlSize">{`(${product.weight} ${product.weight_unit})`}</span>
                  </div>
                  <div className="itemDiv">
                    <h4 className="customPriceHead">{`₹ ${product.final_price}`}</h4>
                    <span className="originalPrice">{`₹ ${product.price}`}</span>
                  </div>
                  <div className="itemDiv">
                    <Button
                      variant="contained"
                      disabled={!product.is_in_stock}
                      className={
                        product.is_in_stock ? "customButton" : "disabledCss"
                      }
                    >
                      {product.is_in_stock ? "Add To Cart" : "Out Of Stock"}
                    </Button>
                  </div>
                </Grid>
                <Grid className="gray-color" item md={1}>
                  {product.rating ? `${product.rating}★` : ""}
                </Grid>
              </Grid>
            </Grid>
          ))}
        </Grid>
      )}
      <Grid className="mt-8" spacing={2} container>
        <Grid item xs={4} md={2}>
          <FormControl className={classes.margin}>
            {/* <InputLabel htmlFor="demo-customized-textbox">Category</InputLabel> */}
            <span className="categoryTitle">Category</span>
            <BootstrapInput
              readOnly
              value={(props.activeCategory) ? props.activeCategory.name : ''}
              id="demo-customized-textbox"
            />
          </FormControl>
        </Grid>
        <Grid className="pt-15" item xs={3} md={1}>
          <div>
            <Button
              size="small"
              className="text-capitalize"
              aria-controls="simple-menu"
              aria-haspopup="true"
              variant="outlined"
              color="primary"
              onClick={handleClick}
            >
              Change
            </Button>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              {
                props.categoryList.length && props.categoryList.map(item => (
                  <MenuItem key={item.category_id} onClick={() => {toggleCategory(item.category_id)}}>{item.category_name}</MenuItem>
                ))
              }
            </Menu>
          </div>
        </Grid>
        <Grid className="pt-15" item xs={5} md={2}>
          <Button className="text-capitalize" size="small" variant="outlined" onClick={toggleLimit}>
            {!limit ? `[-] View Less` : `[+] View More`}
          </Button>
        </Grid>
      </Grid>
    </div>
  );
};

export default Product;
