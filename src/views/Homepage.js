import React, { Component } from "react";
import { Row, Col, Container } from "react-bootstrap";
import axios from "axios";
import Category from "./components/category/Category.js";
import Product from "./components/product/Product.js";
import "./Homepage.css";

class Homepage extends Component {
  state = {
    loader: false,
    categoryList: [],
    productList: [],
    activeCategory: false
  };

  componentDidMount() {
    this.fetchCategoryList();
  }

  fetchCategoryList = () => {
    axios
      .get(
        `https://backend.ustraa.com/rest/V1/api/homemenucategories/v1.0.1?device_type=mob`
      )
      .then((res) => {
        const categoryResponse = res.data;
        const { category_list, product_list } = categoryResponse;
        const firstDefaultCategory = {id: category_list[0].category_id, name: category_list[0].category_name}
        this.setState({ categoryList: category_list });
        this.setState({ productList: product_list.products });
        this.setState({ activeCategory: firstDefaultCategory })
      });
  }

  fetchSpecificCategoryList = (id) => {
    axios
      .get(
        `https://backend.ustraa.com/rest/V1/api/catalog/v1.0.1?category_id=${id}`
      )
      .then((res) => {
        const categoryResponse = res.data;
        const { products } = categoryResponse;
        console.log('response', products)
        if (products) {
          this.setState({ productList: products });
          const categoryFilter = this.state.categoryList.find(item => { return item.category_id === id })
          this.setState({ activeCategory: {id: categoryFilter.category_id, name: categoryFilter.category_name}})
        }
      });
  }

  render() {
    const {categoryList, productList, activeCategory} = this.state
    return (
      <Container>
        <Row className="commonPadding">
          <Col md="12">
            <h3>Our Products</h3>
          </Col>
        </Row>
        <Row>
          <Category fetchProduct={this.fetchSpecificCategoryList} categoryList={categoryList} />
        </Row>
        <Row>
          <Product fetchProduct={this.fetchSpecificCategoryList} categoryList={categoryList} productList={productList} activeCategory={activeCategory} />
        </Row>
      </Container>
    );
  }
}

export default Homepage;
